import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:webview_flutter/webview_flutter.dart';

class MainPageIos extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => MainPageIosState();
}

AsyncSnapshot<WebViewController> controllerGlobal;

class MainPageIosState extends State<MainPageIos>
    with TickerProviderStateMixin {
  final webPageUrl = "https://oawo.com/";
  AnimationController _loaderRotationController;
  double _loaderRotation = 0;
  bool _preloaderVisible = true;
  final Completer<WebViewController> _controller =
      Completer<WebViewController>();

  void _onPageLoadStart(String value) {
    Timer(Duration(seconds: 15), () {
      setState(() {
        _preloaderVisible = false;
      });
    });
  }

  void _onPageLoadComplete(String value) {
    setState(() {
      _preloaderVisible = false;
    });
  }

  Widget _webPageBuilder() {
    return Builder(builder: (BuildContext context) {
      return Padding(
        padding: EdgeInsets.only(top: 35.0),
        child: WebView(
          initialUrl: "$webPageUrl",
          javascriptMode: JavascriptMode.unrestricted,
          gestureNavigationEnabled: true,
          onWebViewCreated: (WebViewController webViewController) {
            _controller.complete(webViewController);
          },
          onPageStarted: _onPageLoadStart,
          onPageFinished: _onPageLoadComplete,
        ),
      );
    });
  }

  @override
  void initState() {
    super.initState();

    _loaderRotationController = AnimationController(
      vsync: this,
      duration: Duration(
        milliseconds: (1000 * 60),
      ),
    );
    _loaderRotationController.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        _loaderRotationController.repeat();
      } else if (status == AnimationStatus.dismissed) {
        _loaderRotationController.forward();
      }
    });
    _loaderRotationController.addListener(() {
      setState(() {
        _loaderRotation = _loaderRotationController.value * 360;
      });
    });
    _loaderRotationController.forward();
  }

  @override
  void dispose() {
    _loaderRotationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Future<bool> _onBack() async {
      controllerGlobal.data.goBack();
      return Future.value(false);
    }

    var screen = MediaQuery.of(context).size;
    return WillPopScope(
      onWillPop: () => _onBack(),
      child: Scaffold(
        body: Stack(fit: StackFit.expand, children: [
          _webPageBuilder(),
          Visibility(
            visible: _preloaderVisible,
            child: Container(
              width: screen.width,
              height: screen.height,
              color: Colors.white,
              child: Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Padding(
                      padding: EdgeInsets.symmetric(vertical: 10.0),
                      child: Container(
                        width: screen.width / 1.8,
                        height: 50.0,
                        child: SvgPicture.asset("assets/images/oawo.svg",
                            color: Colors.red,
                            semanticsLabel: 'A red up arrow'),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(vertical: 10.0),
                      child: Container(
                        width: 30.0,
                        height: 30.0,
                        child: Transform.rotate(
                          angle: _loaderRotation,
                          child: SvgPicture.asset("assets/images/oval.svg",
                              color: Colors.red,
                              semanticsLabel: 'A red up arrow'),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          )
        ]),
        floatingActionButton: FutureBuilder(
          future: _controller.future,
          builder: (BuildContext context,
              AsyncSnapshot<WebViewController> controller) {
            controllerGlobal = controller;
            return Row(
              children: [
                Visibility(
                  visible: !_preloaderVisible,
                  child: FloatingActionButton(
                    onPressed: () {
                      controller.data.goBack();
                    },
                    backgroundColor: Theme.of(context).primaryColor,
                    elevation: 0.0,
                    child: Icon(Icons.chevron_left),
                  ),
                ),
              ],
            );
          },
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.startFloat,
      ),
    );
  }
}
