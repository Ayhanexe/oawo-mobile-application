import 'package:flutter/material.dart';
import 'package:oawo/screens/mainPageAndroid.dart';
import 'package:oawo/screens/mainPageIOS.dart';
import 'dart:io' show Platform;


void main() {
  runApp(OAWOApp());
}

class OAWOApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'oawo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      routes: {
    "/": (_) => checkPlatform()
    });
  }
}

Widget checkPlatform() {
  if(Platform.isIOS) {
    return MainPageIos();
  }
  else if(Platform.isAndroid) {
    return MainPageAndroid();
  }
  return MainPageAndroid();
}
